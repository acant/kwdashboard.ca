# frozen_string_literal: true

require 'date'
require 'oj'
require 'sugar_utils'
require 'haml'
require 'yaml'
require 'active_support/time'

require_relative 'geokit_geojson'
require_relative 'arc_gis_hub'
require_relative 'wrdashboard/arc_gis_hub_config'

class WRDashboard
  # @param glob_pathname [String, Pathname]
  # @param command [String]
  # @param db_pathname [Pathname]
  # @param options [Hash]
  # @option options [Boolean] :verbose
  # @option options [String, Pathname] :output
  # @option options [String, Array<String>] :global_flags
  # @option options [String, Array<String>] :command_flags
  #
  # @return [void]
  def self.glob_pluto(glob_pathname, command, db_pathname, options = {})
    db_pathname.mkpath

    Pathname(options[:output]).mkpath if options[:output]

    Pathname(glob_pathname).glob('*.ini') do |ini_filename|
      puts ">>>>>>>>>> #{ini_filename} >>>>>>>>>>"
      system(
        [
          'pluto',
          options[:global_flags],
          options[:verbose] ? '--verbose' : '--quieter',
          command,
          "--dbpath=#{db_pathname}",
          "--dbname='pluto.db'",
          options[:command_flags],
          options[:output] ? ['--template=wrdashboard', "--output=#{options[:output]}"] : nil,
          ini_filename
        ].flatten.compact.map(&:to_s).join(' ')
      )
    end
  end

  # @param source [OpenStruct]
  # @param options [Hash]
  # @option options [Boolean] :append_years
  # @option options [Pathname] :geojson_dir
  # @option options [Pathname] :html_dir
  #
  # @return [void]
  def self.slice_by_date(source, options = {})
    geojson_directory_prefix = options[:geojson_dir].join(source.type)
    html_directory_prefix    = options[:html_dir].join(source.type)

    puts "  * #{source.type} > #{source.label}"

    haml =
      Haml::Engine.new(
        SugarUtils::File.read('source/geojson_feature.html.haml')
      )

    # Read existing features, if new data should be appended.
    features_by_year = {}
    if options[:append_years]
      geojson_directory_prefix.glob('*.geojson') do |pathname|
        filename               = pathname.to_s
        year                   = /[0-9]+/.match(filename).to_s.to_i
        features_by_year[year] = SugarUtils::File.read_json(filename)
      end
    end

    last_year = nil
    last_id   = nil
    source.geojson_features.each do |feature|
      year      = source.geojson_feature_year(feature)
      id        = source.geojson_feature_id(feature)
      join_data = source.join_data(feature)

      # HACK: Disable this for the moment, because it seems to be eliminating
      # building permits at different locations.
      # TODO: Review how to de-duplicate different datasets. [ASC 2020-06-09]
      # Skip if this feature is a repeat of the last feature.
      # next if year == last_year && id == last_id
      last_year     = year
      last_id       = id

      # Skip if this feature should be excluded by its properties.
      next if source.geojson_feature_exclude?(feature)

      escaped_id    = id.to_s.tr(' ', '_')
      html_url      = "/#{source.type}/#{year}/#{escaped_id}.html"
      html_filename = html_directory_prefix.join("#{year}/#{escaped_id}.html").to_s
      html          = haml.render(
        Object.new,
        permalink_href: html_url,
        title:          "#{source.title}: #{id}",
        geojson:        feature,
        join_data:      join_data
      )

      begin
        SugarUtils::File.write(html_filename, html)
      rescue
        begin
          puts '.'
          SugarUtils::File.write(html_filename, html)
        rescue
          pp "failed #{html_filename} #{e}"
        end
      end
      # HACK: Disable the gzip for the moment. I suspect that it is creating
      # more files than Gitlab Pages deploy can handle.
      # `gzip --to-stdout #{html_filename} > #{html_filename}.gz`

      features_by_year[year] ||= []
      feature['properties'] = { url: html_url }

      # If this dataset has a marker, then find the centre of the feature's
      # geometry.
      if source.marker?
        feature['geometry']['coordinates'] =
          Geokit::GeoJSON.decode(feature).centroid.to_geojson_coordinate
        feature['geometry']['type']        = 'Point'
      end

      features_by_year[year].push(feature)
    end

    features_by_year.each_pair do |k, v|
      filename = geojson_directory_prefix.join("#{k}.geojson").to_s
      SugarUtils::File.write_json(filename, v, timeout: 30)
      `gzip --to-stdout #{filename} > #{filename}.gz`
    end

    nil
  rescue => exception
    puts '  ======================================================='
    puts '  FAIL #slice_by_date'
    puts "  #{exception}"
    puts '  ======================================================='
  end

  # @param source_data_filename [Pathname]
  # @param options [Hash]
  # @option options [Pathname] :geojson_dir
  # @option options [Pathname] :html_dir
  # @option options [Pathname] :download_dir
  #
  # @return [void]
  def self.build_geojson(source_data_filename, options = {})
    geojson_dir  = options[:geojson_dir]
    html_dir     = options[:html_dir]
    download_dir = options[:download_dir]

    arc_gis_hub_config = ArcGISHubConfig.new(source_data_filename, download_dir)
    collections = []
    arc_gis_hub_config.map_marker_collections.each do |collection|
      append_years = false

      collection.datasets.each do |dataset|
        slice_by_date(
          dataset,
          append_years: append_years,
          geojson_dir:  geojson_dir,
          html_dir:     html_dir,
        )
        append_years = true
      end

      collections.push(
        url:  "/map/#{collection.type}/index.json",
        icon: collection.icon
      )

      type_geojson_dir = geojson_dir.join(collection.type)
      # Create the index of all the years of this type.
      SugarUtils::File.write_json(
        type_geojson_dir.join('index.json'),
        type_geojson_dir.glob('*.geojson').map { |x| "/#{x.relative_path_from(html_dir)}" }
      )
    end

    # Create the index of all the collections.
    SugarUtils::File.write_json(
      geojson_dir.join('index.json').to_s, collections
    )
  end

  # @param source_data_filename [Pathname]
  # @param options [Hash]
  # @option options [Pathname] :html_dir
  # @option options [Pathname] :download_dir
  #
  # @return [void]
  def self.filter_geohub_dataset(source_data_filename, options = {})
    html_dir                     = options[:html_dir]
    download_dir                 = options[:download_dir]

    arc_gis_hub_config = ArcGISHubConfig.new(source_data_filename, download_dir)

    render_haml(
      Pathname('source/data_sources.html.haml'),
      Pathname("#{html_dir}/data_sources/index.html"),
      displayed_datasets:            arc_gis_hub_config.map_marker_datasets,
      overlayed_datasets:            arc_gis_hub_config.overlay_datasets,
      openstreetmap_import_datasets: arc_gis_hub_config.openstreetmap_import_datasets,
      rejected_datasets:             arc_gis_hub_config.rejected_datasets,
      unfiltered_geojson_datasets:   arc_gis_hub_config.unfiltered_geojson_datasets,
      unfiltered_datasets:           arc_gis_hub_config.unfiltered_other_datasets
    )
  end

  def self.render_haml(template_pathname, output_pathname, options = {})
    output_pathname.dirname.mkpath

    haml = Haml::Template.new({}) { SugarUtils::File.read(template_pathname.to_s) }
    html = haml.render(
      options.delete(:object) || Object.new,
      options
    )
    SugarUtils::File.write(output_pathname.to_s, html)
  end

  def self.render_hamls(*hamls)
    hamls.each { |args| render_haml(*args) }
  end
end
