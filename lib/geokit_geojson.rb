# frozen_string_literal: true

require 'geokit'

module Geokit
  # Monkey patch GeoKit::LatLng to include both:
  # * converting to a GeoJSON coordinate Array
  # * return a centroid, which is just the point itself
  class LatLng
    # Convert Geokit:LatLng to a GeoJSON point array, and truncate those values
    # to the specified precision. Default to 6 digits, has suggested in the
    # GeoJSON RFC.
    # @see https://tools.ietf.org/html/rfc7946#section-11.2
    # @see https://en.wikipedia.org/wiki/Decimal_degrees
    #
    # Allow the precision to be overridden.
    #
    # @overload #to_geojson_coordinate
    #   Default to a precision of 6 digits.
    # @overload #to_geojson_coordinate(precision)
    #   @param precision [Integer]
    #
    # @return [Array<Integer, Integer>]
    def to_geojson_coordinate(precision = 6)
      [lng.truncate(precision), lat.truncate(precision)]
    end

    # @return [Geokit::LatLng]
    def centroid
      self
    end
  end

  # New Geokit shape which contains multiple polygons.
  # Currently, only supports finding the centre of all of the polygons.
  class MultiPolygon
    @polygons = []

    # @param polygons [Geokit::Polygon]
    #
    # @return [Geokit::MultiPolygon]
    def initialize(polygons)
      @polygons = polygons
    end

    # Find the centroid of the centroids of all the polygons.
    #
    # @return [Geokit::LatLng]
    def centroid
      centroids = @polygons.map(&:centroid)

      case centroids.count
      when 1 then centroid[0]
      when 2 then centroids[0].midpoint_to(centroids[1])
      else
        Geokit::Polygon.new(centroids).centroid
      end
    end
  end

  # Wrapper for handling GeoJSON feature geometries through the Geokit shape
  # objects. This was inspired by the rgeo-geosjon gem, which implements the
  # same idea of the rgeo gem.
  #
  # @see https://github.com/rgeo/rgeo
  # @see https://github.com/rgeo/rgeo-geojson
  class GeoJSON
    # Decode the geometry of a GeoJSON feature hash into a Geokit shape object.
    #
    # @param feature [Hash]
    #
    # @return [Geokit::LatLng]
    # @return [Geokit::Polygon]
    # @return [Geokit::MultiPolygon]
    def self.decode(feature)
      fail(
        "Geokit::GeoJSON.decode requers a feature: #{feature}"
      ) unless feature['type'] == 'Feature'

      geometry    = feature['geometry']
      coordinates = geometry['coordinates']

      case geometry['type']
      when 'Point'
        coordinate_to_latlng(coordinates)
      when 'MultiPoint'
        # HACK: Assuming, for the moment, that using on the first coordinate in
        # the MultiPoint is acceptable
        # TODO: A MultiPoint class should be extracted, when can find the
        # centroid of 1 or more points.
        coordinate_to_latlng(coordinates.first)
      when 'Polygon'
        coordinates_to_polygon(coordinates.first)
      when 'MultiPolygon'
        Geokit::MultiPolygon.new(
          coordinates.map { |x| coordinates_to_polygon(x.first) }
        )
      else
        fail("Geokit::GeoJSON.decode could not decode: #{geometry}")
      end
    end

    ############################################################################

    # @param coordinate Array<Integer, Integer>
    #
    # @return [Geokit::Latlng]
    def self.coordinate_to_latlng(coordinate)
      Geokit::LatLng.new(coordinate[1], coordinate[0])
    end

    # @param coordinates [Array<Array<Integer, Integer]>]
    #
    # @return [Geokit::Polygon]
    def self.coordinates_to_polygon(coordinates)
      Geokit::Polygon.new(
        coordinates.map { |coordinate| coordinate_to_latlng(coordinate) }
      )
    end
  end
end
