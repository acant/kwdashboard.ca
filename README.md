# wrdashboard.ca

Eventually, I hope that this will contain the code for a dashboard which
aggregates news/activities/information from [Kitchener-Waterloo](https://en.wikipedia.org/wiki/Tri-Cities_(Ontario))
and the surrounding [Region of Waterloo](http://www.regionofwaterloo.ca/en/index.asp)([wikipedia](https://en.wikipedia.org/wiki/Regional_Municipality_of_Waterloo)).

This would eventually live at [wrdashboard.ca](http://wrdashboard.ca/).

But to start with we will collect the links and information that would
eventually get aggregated.

## Building the website

The website will be automatically re-built by [Gitlab CI](https://docs.gitlab.com/ee/ci/) and pushed to [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). If you want to build locally there are several commands you can use.

First make sure that your ruby environment is setup, with bundler installed.  Then install the required gems:

```
bundle install
```

You can download the feeds, and re-build all of the pages locally by running:

```
bundle exec rake
```

If you see a failure during the building, you can run the build with more
output with:
```
VERBOSE=true bundle exec rake
```

Finally, once the pages are built you can run a locally server so that you can
see how those pages will be displayed:
```
bundle exec rake server
```

You should then be able to view the locally built pages at http://localhost:4587

## License

Unless otherwise stated please assume the following license for:

* software, [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)
* content and other media, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
* datasets, [ODbL v1.0](http://opendatacommons.org/licenses/odbl/1.0/)

## Aggregation lists
* [Articles](https://gitlab.com/acant/wrdashboard.ca/blob/master/data/articles.ini)
* [Audio](https://gitlab.com/acant/wrdashboard.ca/blob/master/data/audio.ini)
* [Video](https://gitlab.com/acant/wrdashboard.ca/blob/master/data/video.ini)
* [Reddit](https://gitlab.com/acant/wrdashboard.ca/blob/master/data/reddit.ini)
* [Repositories](https://gitlab.com/acant/wrdashboard.ca/blob/master/data/repositories.ini)

## Existing Aggregators

* [+365 Things to do in Kitchener-Waterloo](http://365-kw.com/), no updates in
  2014
* [Actively Guide](https://guide.goactively.com/tag/kitchener-waterloo/)
* [Creative Capital of Canada](https://www.creativecapitalofcanada.ca/)
* [DownTownKitchener](http://downtownkitchener.ca)
* [etrigg](https://etrigg.com/)
* [EventBrite](https://www.eventbrite.ca/d/canada--waterloo/events/?mode=search)
* [Explore Waterloo Region](http://www.explorewaterlooregion.com/)
* [Fun And You - Kitchener](http://www.funandyou.com/things-to-do-in-kitchener-ontario.htm)
* [Grand River Conservation Authority](https://www.grandriver.ca/en/our-watershed/Maps-and-data.aspx)
* [Kitchener City Calendar](http://kitchener.ca/en/calendar):
* [Kitchener Events](http://kitchenerevents.ca)
* [Kitchener Post - Events](https://www.kitchenerpost.ca/kitchener-on-events/)
* [@KWawesome](https://twitter.com/KWAwesome)
* [KW Music](https://twitter.com/KW_Music)
* [KW Tonite](https://kwtonite.com)
* [Live Local KW](http://www.livelocalkw.com/)
* [Playticipate](https://playticipate.com/kitchener-waterloo/kids-programs-and-activities)
  - collection of of kids activities and events in Kitchener-Waterloo
* [Podcast list](http://pnijjar.freeshell.org/2014/wr-podcasts/) collected by [Paul Nijjar](http://pnijjar.freeshell.org/2014/wr-podcasts/)
* [RAW Artists](https://www.rawartists.org/kitchener)
* [The Record Calendar](https://www.therecord.com/waterlooregion-events/)
* [Stuff To Do With Your Kids in KW](http://stuftodowithyourkidsinkw.blogspot.ca)
* [TicketFi](https://ticketfi.com/)
  - local ticketing service, which is used by the universities
  - does not seem to have an aggregation of events
* [TL;WR](https://www.alexkinsella.com/tlwr/)
* [Todo Canada](https://cse.google.ca/cse?cx=partner-pub-8228390839633607:1281149376&ie=UTF-8&q=kitchener&sa=Search#gsc.tab=0&gsc.q=kitchener&gsc.page=1)
* [WatCamp](http://watcamp.com)
  - [wiki page](http://sobac.com/wiki/index.php/WatCamp) documenting the sources and process they use for populating the calendar
* [Waterloo Chronicle - Events](https://www.waterloochronicle.ca/waterloo-on-events/)
* [World Web](http://www.kitchener-waterloo.worldweb.com/)
* [Region of Waterloo](http://row.calendar.esolutionsgroup.ca/Module.aspx?PMID=c7f6b659-f8b2-4c2f-bf35-d908c4f1e77c&_mid_=17548)
* [Why Waterloo](http://whywaterloo.ca/)
* [WR Blogs](https://wrblogs.samnabi.com/) maintained by [Sam Nabi](https://samnabi.com/) and tweeted at [@wrblogs](https://twitter.com/wrblogs)
* [Waterloo Tech News](https://www.waterlootechnews.com/)
* [Winterloo](https://www.winterloofestival.ca/)
* [WPL Historical Image Collection](http://images.ourontario.ca/waterloo/)
* [WPL Link collection](https://www.wpl.ca/services/meet-your-waterloo)
* [AllEventsIn: Waterloo](https://allevents.in/waterloo/#)
* [AllEventsIn: Kitchener](https://allevents.in/kitchener#)
* [Grand River Blues Society](https://www.grandriverblues.org/)
* Reddit podcast lists
  - [Any local blogs worth checking out?](https://www.reddit.com/r/kitchener/comments/7v4d7j/any_local_blogs_worth_checking_out/)
  - [Any podcaters in rwaterloo](https://www.reddit.com/r/waterloo/comments/75b127/any_podcasters_in_rwaterloo/)
* [Roadside Throughts](https://roadsidethoughts.com/on/wilmot-centre-nearby.htm)
* [Pentalocal](https://www.pentalocal.ca/on/kitchener/mill-courtland-community-centre/)
* [Black Owned K-W](http://acbnetworkwr.com/index.php/black-owned-kw/)
* [Woolwich Township events](https://calendar.woolwich.ca/default/Month)
* [SnapdKW Upcoming](https://kitchenerwaterloo.snapd.com)

## Twitter lists/hashtags

* [#KWAwesome](https://twitter.com/search?q=%23KWAwesome)
* [#MappingWR](https://twitter.com/hashtag/MappingWR)
* [#WRAwesome](https://twitter.com/search?q=%23WRAwesome)
* [#DTK](https://twitter.com/search?q=%23DTK)
* [#WatReg](https://twitter.com/search?q=%23WatReg)
* [#WRInnovation](https://twitter.com/hashtag/WRInnovation)
* [KWLocal list](https://twitter.com/wftl/lists/kwlocal) by [Marcel Gagné](https://twitter.com/wftl)
* [Waterloo-Region list](https://twitter.com/acitta/lists/waterloo-region) by [Gary Walsh](https://twitter.com/acitta)
* [Best Kitchener Twitters](https://twitter.com/HospiceWaterloo/lists/best-kitchener-twitters) by [Hospice Waterloo](https://twitter.com/HospiceWaterloo)
* [#snowbilityWR](https://twitter.com/hashtag/snowbilityWR)
* [#WRsquad list](https://twitter.com/i/lists/1334874037294665728)
* [Waterloo Region list](https://twitter.com/i/lists/190328999)
* [Waterloo Region News](https://twitter.com/i/lists/1283762222708260864)
* [#WRDSB_BookStudy_BDW](https://twitter.com/hashtag/WRDSB_BookStudy_BDW)
* [#KWhiphop](https://twitter.com/hashtag/KWhiphop)
* [#kwmusic](https://twitter.com/hashtag/kwmusic)

## Newspapers/Magazines

* [Kitchener Citizen](http://www.kitchenercitizen.com/)
* [The Record](http://www.therecord.com/waterlooregion/)
* [Kitchener Post](http://www.kitchenerpost.ca/kitchener-on/)
* [Snapd - Kitchener-Waterloo](https://kitchenerwaterloo.snapd.com/)
* [Waterloo Chronicle](http://www.waterloochronicle.ca/waterloo-on/)
* [Grand](http://www.grandmagazine.ca/)
* [Idea Exchange](https://ideaexchange.org/)
* [Inside the Perimeter](https://insidetheperimeter.ca/)
* [The Community Edition](https://communityedition.ca/)
* [Stare City Guide](http://www.starecity.com/)
* [Good Company Magazine](http://goodcomagazine.com/)
* [Observer Extra](https://observerxtra.com/)
* [Textile](https://textilekw.com/)
* [Blueprint Magazine](https://blueprintmagazine.ca/)
* [Good Company Magazine](http://goodcomagazine.com/)
* [WATSnew](https://www.homerwatson.on.ca/about/magazine/)
* [Wilmot Post](https://wilmotpost.ca/)
* [New Hamburg Independent](https://www.newhamburgindependent.ca/)
* [Ayr News](http://ayrnews.ca/)
* [Waterloo Region Rural Post](https://wrruralpost.com/)
* [Wellington Advertiser](https://www.wellingtonadvertiser.com/)

The Waterloo Chronicle also has a [list of newspapers](https://www.waterloochronicle.ca/community-static/3907116-kitchener-waterloo-newspapers).

## Radio and Television
* [570 News](http://www.570news.com/)/[Kitchener Today](https://www.kitchenertoday.com/)
* [CBC Kitchener-Waterloo](http://www.cbc.ca/kitchener-waterloo/)
* [Radio Laurier](http://radiolaurier.com/)
* [91.5 The Beat](https://915thebeat.com/)
* [CHYM 96.7](https://www.chymfm.com/)
* [Midtown Radio](https://midtownradiokw.airtime.pro/#)

## Publishers
* [Pandora Press](http://bookshop.pandorapress.com/)

## Articles and Writing

## Wikipedia

* [Category: Regional Municipality of Waterloo](https://en.wikipedia.org/wiki/Category:Regional_Municipality_of_Waterloo)
  - [Category: Waterloo Region](https://en.wikipedia.org/wiki/Category:Waterloo_Region)
* [Category: Populated places in Waterloo Region](https://en.wikipedia.org/wiki/Category:Populated_places_in_Waterloo_Region)
* [Media in Waterloo Region](https://en.wikipedia.org/wiki/Media_in_Waterloo_Region)
* [Regional Municipality of Waterloo](https://en.wikipedia.org/wiki/Regional_Municipality_of_Waterloo)
* [Category: Communities in Waterloo Region](https://en.wikipedia.org/wiki/Category:Communities_in_Waterloo_Region,_Ontario)
* [The Royal Highland Fusiliers of Canada](https://en.wikipedia.org/wiki/The_Royal_Highland_Fusiliers_of_Canada)
* [31 Combat Engineer Regiment](https://en.wikipedia.org/wiki/31_Combat_Engineer_Regiment_(The_Elgins))
* [Category:Populated places in Waterloo Region](https://en.wikipedia.org/wiki/Category:Populated_places_in_Waterloo_Region)
* [Waterloo Pioneer Memorial Tower](https://en.wikipedia.org/wiki/Waterloo_Pioneer_Memorial_Tower)
* [118th (North Waterloo)](https://en.wikipedia.org/wiki/118th_(North_Waterloo)_Battalion,_CEF)
* [Waterloo County, Ontario](https://en.wikipedia.org/wiki/Waterloo_County,_Ontario)

## OpenStreetMap wiki

* [Kitchener-Waterloo](https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo)
* [Kitchener-Waterloo/Import](https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo/Import)
* [Kitchener-Waterloo mapping party](https://wiki.openstreetmap.org/wiki/Kitchener-Waterloo_mapping_party)(2010)
* [Category:Users in Kitchener-Waterloo](https://wiki.openstreetmap.org/wiki/Category:Users_in_Kitchener-Waterloo)
* [Cambridge, Ontario](https://wiki.openstreetmap.org/wiki/Cambridge,_Ontario)
* [Category:Users in Cambridge](https://wiki.openstreetmap.org/wiki/Category:Users_in_Cambridge)

## Books & Authors

* [Ara the Star Engineer](https://www.arastarengineer.com/en)
* [Andrew Hunt](https://www.authorandrewhunt.com/)
* [Emily Urquhart](http://emilyurquhart.ca/)

## Musicians

* [Lorna Heidt](https://www.facebook.com/lorna.heidt)
* [Rufus John](https://rufusjohn.bandcamp.com/)
* [John Orpheus](https://www.johnorpheus.com/)
* [Karen Sunabacka](http://www.sunabacka.com/)
* [Embassy](https://kingsofthenorth.bandcamp.com/)

## Artist

* https://www.trishaabe.com/
* https://www.stephanieboutari.com/
* https://jessekoreck.com/
* https://www.simonepatricia.com/
* [Afterschool Arcade](https://taplink.cc/afterschoolarcade)

## Documentaries

* [Toxic Time Bomb](https://www.shebafilms.com/toxic-time-bomb-2020/)  about Agent Orange in Elmira

## Military Units
* [units](http://army-armee.forces.gc.ca/en/central/units-formations.page) in Ontario
* [The Royal Highland Fusiliers of Canada](http://army-armee.forces.gc.ca/en/royal-highland-fusiliers-canada/index.page)
* [31 Combat Engineer Regiment](http://army-armee.forces.gc.ca/en/31-combat-engineer-regiment/index.page)

## Organizations with activities/events/calendars

These organizations have calendars. We hope for iCal formatted data that can be
read and aggregated easily, but this is mostly not the case. Scrapping or
manual collection will be necessary in most cases.

* [Accelerator Centre](http://acceleratorcentre.com/)
* [ActOutKW](https://actoutkw.com/)
* [Africa Women's Alliance of Waterloo Region](http://www.afrowomen.org/)
* [Apollo Theatre](https://www.apollotheater.org/)
* [The Athlete Factor](http://www.theathletefactory.ca/)
* [Blackball Comedy](https://twitter.com/blblcomedy)
* [Bookmobike](https://www.bookmobike.com/)
* [Button Factory Arts](http://www.buttonfactoryarts.ca/)
* [Brew Donkey](https://www.brewdonkey.ca/pages/browse-tours)
* [Canada Learning Code: Kitchener-Waterloo](https://www.canadalearningcode.ca/chapters/kitchener-waterloo/)
* [Capacity Canada](https://capacitycanada.ca/)
* [Centre for International Governance Innovation](https://www.cigionline.org/)
* [Centre in the Square](http://centreinthesquare.com/)
* [Cherry Park Neighbourhood Association](https://cherrypark.blogspot.ca/)
* [ClimateActinWR](http://www.climateactionwr.ca/)
* [City of Kitchener Calendar](http://www.kitchener.ca/en/insidecityhall/CurrentAgendasandReports.asp?_mid_=10041)
* [Communitech](https://www.communitech.ca)([calendar](https://www.communitech.ca/get-involved/events/))
* [Concordia Club](https://www.concordiaclub.ca/)
* [Courts & Politics Research Group](http://www.courtsandpolitics.org/)
* [Cycling into the Future](http://www.cyclingintothefuture.com/)
* [Descendants Brewery](https://www.descendantsbeer.com/eventscalendar)
* [Eastern Synod](https://easternsynod.org/calendar)
* [Forest Heights Community Association Inc.](http://www.fhcakitchener.ca/)
* [Flush Ink](http://flushink.net/flushink.html)
* [Grand River Jazz Society](http://www.grandriverjazzsociety.org/)
* [Grand River Rocks Climbing Gym](http://grandriverrocks.com/)
* [Greater KW Chamber of Commerce](http://greaterkwchamber.com/networking-events/)
* [Greenlight Arts](https://www.greenlight-arts.com/)
* [Hive WR](http://hivewr.ca/)
* [Homer Watson House & Gallery](http://www.homerwatson.on.ca)
* [Hydrocut Trail](https://www.thehydrocut.ca/)
* [India Canada Association of Waterloo Region](http://www.icawaterloo.com/)
* [Inter Arts Matrix](https://www.interartsmatrix.ca/)
* [The Jazz Room](http://kwjazzroom.com/)
* [Juicy Yoga](https://juiciyoga.com/events/)
* [Kitchener Horticultural Society](http://www.kitchenerhs.ca/cms/)
* [Kitchener Memorial Auditorium Complex](http://www.theaud.ca/en/index.asp)
* [Kitchener Panthers](http://www.kitchenerpanthers.com/)
* [Kitchener Public Library](http://kpl.org/)
* [Kitchener Rangers](http://kitchenerrangers.com/)
* [Kitchener-Waterloo Centre The Royal Astronomical Society of Canada](http://kw.rasc.ca/)
* [Kitchener-Waterloo Multicultural Centre](https://www.kwmc.on.ca/events/)
* [Kitchener-Waterloo Symphony](http://www.kwsymphony.ca/index.php)
* [Krajewski Gallery](https://www.krajewskigallery.com/shows)
* [KW Art Gallery](http://www.kwag.ca/en/)
* [KW Civitan](http://kwcivitan.ca/)
* [KW Inline](http://kwinline.com/)
* [KW Library of Things](http://kwlot.ca/events/)
* [KW Little Theatre](http://kwlt.org/)
* [KW Glee](http://www.kwglee.com/)
* [KW Gymnastics](https://www.kwgymnastics.ca/)
* [KW Peace and Justice](https://kwpeace.ca/)
* [KW Poetry Slam](http://www.kwpoetryslam.com/)[twitter](https://twitter.com/KWPoetrySlam)
* [KWSQA](https://kwsqa.org/)
* [KWTech](https://kwtechs.carrd.co/)
* [KW Woodworking & Craft Centre](http://www.kwwcc.org/)
* [Kwartz Lab](http://www.kwartzlab.ca/)
* [Langs](https://www.langs.org/)
* [Leadership Waterloo Region](https://www.leadershipwaterlooregion.org/)
* [Liason College of Cullinary Arts - Kitchener Campus](http://www.liaisonkitchener.ca)
* [Lions Club of Kitchener](http://lionsclubofkitchener.com/)
* [Lip Off KW](http://lipoffkw.com/)
* [The Making-Box](http://themakingbox.ca)
* [McDougall Cottage](http://www.mcdougallcottage.ca/en/index.aspx)
* [MPRESS MUSIC](https://www.mpressmusic.ca)
* [Nota Bene Baroque Players](http://www.notabenebaroque.ca/)
* [Open Ears](http://openears.ca/)
* [Open Sesame](https://opensesameshop.com/)
* [Our Place KW](https://www.ourplacekw.ca/)
* [Perimeter Institute for Theoretical Physics](https://www.perimeterinstitute.ca/)
* [Princess Cinema](http://www.princesscinemas.com/)
* [Marti Collective](https://www.marit.ca/)
* [Maxwell's Concert and Events](http://maxwellswaterloo.com/)
* [MT Space](http://mtspace.ca/)
* [Rhapsody Barrel Bar](http://www.rhapsodybarrelbar.com/)
* [REAP Waterloo](http://reapwaterloo.ca/)(Research Entrepreneurs Accelerating Prosperity)
* [REEP Green Solutions](https://reepgreen.ca/)
* [Registry Theatre](http://www.registrytheatre.com/)
* [Schwaben Club](https://kitchenerschwabenclub.com/)
* [Schneider Haus](http://www.schneiderhaus.ca/en/index.aspx)
* [Society of Freethinks](https://sofree.ca/)
* [Spectrum](http://ourspectrum.com/)
* [Starlight Social Club](http://starlightsocialclub.ca/)
* [StartupNorth](http://startupnorth.ca/)
* [Steckle Heritage Farm](http://www.stecklehomestead.ca/)
* [Summer Lights Festival](http://summerlightsfestival.com/)
* [St Jacobs Village](http://www.stjacobsvillage.com/events)
* [SVPWaterloo](http://www.socialventurepartners.org/waterloo-region/)
* [Taste of the Region](http://tasteoftheregion.net/)
* [TravelWiseWR](https://gotravelwise.ca/#/)
* [UWaterloo - Capstone Design](https://uwaterloo.ca/capstone-design/events)
* [Uwaterloo - Computer Science Department](https://cs.uwaterloo.ca/wics/events-activities)
* [UWaterloo - Cryptography, Security, and Privacy (CrySP) Research Group](https://crysp.uwaterloo.ca/)
* [UWaterloo - Global Engagement Program](https://uwaterloo.ca/global-engagement-program/)
* [UWaterloo - Recreaction Committee](https://uwaterloo.ca/recreation-committee/)
* [UWaterloo - Science](https://www.uwaterloo.ca/science/events)
* [Waterloo Aboriginal Education Centre](https://uwaterloo.ca/stpauls/waterloo-aboriginal-education-centre)
* [Waterloo County Rugby Club](https://www.waterloocountyrugby.com/)
* [Waterloo Cycling Club](http://waterloocyclingclub.ca/)
* [Waterloo Dodgeball](https://waterloododgeball.com/)
* [Waterloo Freemasons](http://waterloofreemasons.ca/)
* [Waterloo Global Science Initiative](http://wgsi.org/)
* [Waterloo Historical Society](http://www.whs.ca/)
* [Waterloo Public Library](http://www.wpl.ca/)
* [Waterloo Regional Heritage Foundation](https://www.wrhf.org/en/index.aspx)
* [Waterloo Region Electric Vehicle Association](https://wreva.ca/)
* [Waterloo Region Labour Council](http://www.wrlc.ca/)
* [Waterloo Region Museum](http://www.waterlooregionmuseum.ca/)
* [Waterloo Region Family Network](https://wrfn.info/)
* [Waterloo Region Small Business Centre](https://www.waterlooregionsmallbusiness.com/)
* [Waterloo Wildfire Ringette](http://waterlooringette.com/)
* [Waterloo Warbirds](http://www.waterloowarbirds.com/)
* [Woodside National Historic Site](http://www.pc.gc.ca/en/lhn-nhs/on/woodside)
* [World Religions Conference](https://www.worldreligionsconference.org/)
* [White Owl Native Ancestry Association](https://www.wonaa.ca/)
* [The Whip Boxing](http://www.thewhipboxing.ca/)
* [Velocity](http://velocity.uwaterloo.ca/)
* [YMCA of Cambridge & Kitchener-Waterloo](http://www.ymcacambridgekw.ca/en/index.asp)
* [Zonta](http://www.zontakw.org/)

### People with Events
* [Richard Garvey](http://www.richardgarvey.ca/)

### Homepages without feeds
* [Kelly Gariepy](http://www.kellygariepy.com/)

### Teams

* [Kitchener Dutchmen](http://www.kitchenerdutchmen.com/)
* [KW Titans](http://kwtitans.com/)
* [Tri-City Roller Derby](http://tricityrd.com/)
* [Waterloo Warriors](http://warriors.uwaterloo.ca/)

### List of Organizations to expand later
* [Kitchener Neighbourhood Associations](https://www.kitchener.ca/en/in-your-neighbourhood/neighbourhood-associations.aspx#)
* [Kitchener Swimming pools](http://www.kitchener.ca/en/livinginkitchener/PoolsAndSwimming.asp)
* [Waterloo Neighbourhood Associations](http://www.waterloo.ca/en/living/neighbourhoodassociations.asp)
* [Waterloo Swimming pools](http://www.waterloo.ca/en/gettingactive/swimming.asp)

### Public Transportation

* [Grand River Transit](http://www.grt.ca/)
* [Greyhound](http://greyhound.ca)
* [VIA Rail](http://www.viarail.ca/en)
* [Go Transit](http://www.gotransit.com/)

### Grants available in the Region

* [Festivals and Events Grant](https://www.explorewaterlooregion.com/wp-content/uploads/2019/10/Festivals-and-Events-Grant-Application-Guidelines.pdf)
* [Region of Waterloo Arts Fund](https://artsfund.ca/)

### Open Data sources which cover the region

* [338Canada](https://338canada.com/)
* [Air Quality Ontario](http://www.airqualityontario.com/history/station.php?stationid=26060)
* [arXiv](https://arxiv.org/help/api)
  - https://github.com/eonu/arx
* [Canadian Disaster Database(CDD)](https://www.publicsafety.gc.ca/cnt/rsrcs/cndn-dsstr-dtbs/index-en.aspx)
* [Canadian Institute for Health Information](https://www.cihi.ca/en/access-data-and-reports)
* [Canadian Legal Information Institute](https://www.canlii.org/en)
* [City of Kitchener](http://www.kitchener.ca/en/insidecityhall/open-data.asp)
* [City of Waterloo](http://www.waterloo.ca/en/opendata/index.asp)
* [City of Cambridge GeoHub](http://geohub.cambridge.ca/)
* [Community Data Program: Waterloo Region](https://communitydata.ca/content/waterloo-region)
* [Corporate Mapping](https://www.corporatemapping.ca/)
* [Guelph GeoDataHub](https://geodatahub-cityofguelph.opendata.arcgis.com/)
* [Kitchener GeoHub](https://open-kitchenergis.opendata.arcgis.com/)
* [Libary and Archives Canada](https://www.bac-lac.gc.ca/eng/Pages/home.aspx)
* [Little Sis](https://littlesis.org/)
* [Living Wage Canada](http://www.livingwagecanada.ca/)
* [Lobby Canada](https://lobbycanada.gc.ca/)
* [FoodLinkWR](https://www.foodlinkwr.ca/)
* [Ontario Arts and Artists list](http://www.mtc.gov.on.ca/en/arts/arts.shtml)
* [Ontario Heritage Trust](https://www.heritagetrust.on.ca/en/index.php/pages/tools/data-inventories)
* [Ontario Human Rights Commision](http://www.ohrc.on.ca)
  - how to filter cases which related to the region?
  - Christian Horizons is a particular [example](http://www.ohrc.on.ca/en/ontario-human-rights-commission-v-christian-horizons)
* [Ontario Power Generation Dashboard]()
* [Ontario Open Data Catalogue](https://www.ontario.ca/search/data-catalogue)
  - [Student Demographic Data](https://www.ontario.ca/data/school-information-and-student-demographics)
  - [Long Term Care Home Finder](https://www.therecord.com/news-story/9964464-fischer-hallman-road-project-slated-to-start-in-may/) which includes inspection data
* [Ontario Treaties and Reserves](https://www.ontario.ca/page/map-ontario-treaties-and-reserves)
* [Ontario's Universities](https://ontariosuniversities.ca/resources/data/)
* [Open Canada](https://open.canada.ca/)
  - link to suggest more data sets https://open.canada.ca/en/suggested-datasets
* [Open Food Network](https://openfoodnetwork.ca/)
* [Parkopedia](https://en.parkopedia.ca/parking/kitchener/)
* [Radio Reference: Waterloo County(ON)](https://wiki.radioreference.com/index.php/Waterloo_County_(ON))
* [Region of Waterloo](http://www.regionofwaterloo.ca/en/regionalGovernment/OpenDataHome.asp)
* [University of Waterloo](https://uwaterloo.ca/information-systems-technology/services/open-data)
* [UWSpace](https://uwspace.uwaterloo.ca/), research paper repository
* [University of Waterloo - Weather Station](http://weather.uwaterloo.ca/)
* [WSIB Ontario](http://www.wsibopendata.ca/)
* [GRT](https://www.grt.ca/en/about-grt/open-data.aspx)
* [GRT Performance measures](https://www.grt.ca/en/about-grt/performance-measures.aspx)
* [Outlook Study](https://yourwrrc.ca/rcc/outlook-study/)
* [Hockeystick](https://www.hockeystick.co/), Canadian company growth data
* [WaterlooEDC](https://www.waterlooedc.ca)
* [Waterloo Region Population Report](https://www.regionofwaterloo.ca/en/regional-government/population.aspx)
* [Waterloo Region Housing Catalogue](https://housingcatalogue.regionofwaterloo.ca/Home/SearchMap)
* [Waterloo Startups List](http://waterloo.startups-list.com)
* [Workforce Planning Board of Waterloo Wellington Dufferin](https://www.workforceplanningboard.com/)
* [Sustainable Waterloo Membership](https://www.sustainablewaterlooregion.ca/join-a-program/regional-sustainability-initiative/program-members/)
* [StackShare](https://stackshare.io/)
* [Global Legal Entity Identifier](https://www.gleif.org/en/)

### Non-Open Data sources/aggregators/directories

* https://www.dnb.com/
* https://www.canadacompanyregistry.com/
* https://www.realtor.ca/

### OpenData projects which can be contributed to

These are projects which can be both source of data and place to contributed.

I think it would be worthwhile making documents/tools to make documenting
specific scenarios easy and clear. (e.g., adding a historical plaque to
OpenStreetMaps? add it to wikipedia, wikicommons, OpenPlaques. Found a new
local musician? make sure they are in wikipedia, muscibrainz, freedb, discogs)

* [BookBrainz](https://bookbrainz.org/)
* [Discogs](https://www.discogs.com/values)
* [FreeDB](http://www.freedb.org/)
* [InternetArchive](https://archive.org/index.php)
* [Mapillary](https://www.mapillary.com/)
* [MusicBrainz](https://musicbrainz.org/)
* [OpenCellID](http://opencellid.org/)
* [OpenParliament](https://openparliament.ca/)
* [OpenPlaques](https://openplaques.org/)
* [OpenStreetMaps](https://www.openstreetmap.org/relation/2062151)
* [KartaView](https://kartaview.org)
  - used to be [OpenStreetCam](https://openstreetcam.org/)
* [OpenLibrary](https://openlibrary.org/)
* [PlugShare](https://www.plugshare.com/)
* [Radio Cells](https://www.radiocells.org/)
* [Radio Garden](https://radio.garden/)
* [WikiData](https://www.wikidata.org/wiki/Wikidata:Main_Page)
* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)
  * [Commons:Wiki Loves Monuments](https://commons.wikimedia.org/wiki/Commons:Wiki_Loves_Monuments_2019_in_Canada)
* [Wikipedia](https://en.wikipedia.org/wiki/Main_Page)

### Curated lists
* https://www.greatplacetowork.ca/en/best-workplaces/technology-2019

### Data Feeds which cover the region
* [N2YO.com](https://www.n2yo.com/) satellite visibility
* [LoopNet](https://www.loopnet.com/)
* [Muck Rack](https://muckrack.com/kate-bueckert)

### Maps

* [Little Libraries](http://www.cbc.ca/news/canada/kitchener-waterloo/little-libraries-waterloo-region-guelph-ontario-1.2764550), maintained by CBC
  - https://llkw.ca/honour-roll/
  - https://www.cbc.ca/news/canada/kitchener-waterloo/little-free-libraries-kitchener-waterloo-1.5561561
* [Gender Neutral Bathrooms](http://ourspectrum.com/projects/gender-neutral-restrooms/), maintained by [Spectrum](http://ourspectrum.com/)
* [Mapping Displacement](https://storymaps.arcgis.com/stories/43112521f46a45bcac89ba849f40a0ee)
* [Tri-City Hip-Hop](https://tricityhiphop.com/)
* [Industrial Artifacts](https://www.kitchener.ca/en/resourcesGeneral/Documents/DSD_ECDEV_Industrial_Artifacts-Map.pdf)
* [Kitchener Snow Removal](https://www.kitchener.ca/en/in-your-neighbourhood/snow-removal.aspx) links to 2 maps:
  - [free road sand locations](http://app2.kitchener.ca/app/sandbox/)
  - [road clearing priority](https://www.kitchener.ca/en/in-your-neighbourhood/snow-plow-priorities.aspx)
* [ShopLocalWR](https://juliewitmermaps.com/pages/shoplocalwr)

## Meetups

* [KW Fireside Gatherings](http://kwfireside.cedarcreek.ca/)
* [KWVoIP](http://kwvoip.ca/)

## Annual-ish events

* [Afrofest](http://www.afrowomen.org/afrofest.php)
* [Belmont Village Bestival](https://belmontvillagebestival.com)
* [Communitech 151](http://communitech151.com/)
* [Cruising on King St](https://www.facebook.com/events/1715427935402973)
* [Culture Days](https://culturedays.ca/)
* [Cybercity Conf](https://www.cybercityconf.io/)
* [Doors Open Waterloo Region](http://www.regionofwaterloo.ca/en/doorsopenwaterloo.asp)
* [ENDURrun](https://www.endurrun.com/)
* [EverAfter Festival](https://www.everafterfest.com/)
* [Equithon](https://equithon.org/)
* [Fluxible](http://www.fluxible.ca/)
* [Grand River Bass Derby](http://www.grandriverbassderby.ca/)
* [Grand River Water Walk](https://www.grandriverwaterwalk.com/)
* [HamFest](http://www.hamfest.on.ca/)
* [Hold the Line](https://www.holdthelinewr.org/)
* [Impact Festival](http://mtspace.ca/impact-17)
* [Inspiring Women Event](https://inspiringwomenevent.com/)
* [Kitchener Blues Festival](https://www.kitchenerbluesfestival.com/)
* [Kitchener Ribfest & Craft Beer Show](http://www.kitchenerribandbeerfest.com/)
* [Kitchener Waterloo Film Festival](https://www.facebook.com/kwfilmfest/)
* [KOI Music Festival](https://twitter.com/koimusicfest)
* [Kultran World Music Festival](http://www.nerudaarts.ca/kultrun)
* [KW Comedy Festival](http://kw-comedy.com/)
* [KW Craftoberfest](http://www.kwcraftoberfest.com/)
* [KW Linux Fest](http://www.kwlinuxfest.ca/)
* [KW Multicultual Festival](https://www.kwmc.on.ca/festival-2017)
* [KW Veg Fest](https://kwvegfest.ca/)
* [Maker Expo](http://www.makerexpo.ca/)
* [Mela](http://www.melawaterloo.com/)
* [Movies in the Park, Waterloo Park](http://parkmovies.ca/#home)
* [Nightshift](http://nightshiftwr.ca/)
* [Oktoberfest](http://www.oktoberfest.ca/)
* [OpenDoors Waterloo Region](https://www.doorsopenontario.on.ca/waterloo-region)
* [Pupusas Festival](http://www.wherevent.com/detail/Salvadorian-Canadian-Community-PUPUSAS-FESTIVAL)
* [Rock Revival Kitchener](https://www.rockrevivalkitchener.com)
* [Rock & Rumble](http://www.kitchenerevents.ca/whats-on/events?eventID=152)
* [Schneider Creek Porch Party](http://scporchparty.weebly.com/)
* [Starcon](https://starcon.io/)
* [Strummerfest](https://www.strummerfest.ca/)
* [Tri-pride](http://tri-pride.ca/)
* [True North Waterloo](http://truenorthwaterloo.com/)
* [Waterloo Jazzfest](https://www.waterloojazzfest.com/)
* [Waterloo Buskers](http://www.waterloobuskers.com/)
* [Waterloo Rib Festival](https://northernheatribseries.ca/waterloo/)
* [Waterloo Staff Conference](https://uwaterloo.ca/staff-conference/)
* [WatITis](https://uwaterloo.ca/watitis/)
* [Zonata Film Festival](https://kwzontafilmfestival.com/)

## Dashboard implementation ideas

* https://twitter.com/davewiner/status/955202566845067265

### Existing Projects to consider using

* http://dat*data.com/
* https://localwiki.org/
  - https://localwiki.org/kitchener/
* elm tree project
* https://github.com/feedreader/pluto
* https://github.com/Aupajo/almanack
* https://github.com/theirishpenguin/ical*merger
* [Calagator](http://calagator.org/) Portland tech calendar
* https://rsip22.github.io/blog/my-project-with-outreachy.html
* https://rsip22.github.io/blog/my-contribution-to-github-icalendar.html
* https://github.com/nhnent/tui.calendar
* https://docs.rsshub.app/en/
* https://github.com/huginn/huginn
* http://feed43.com/
* https://arxiv.org/help/api
* https://granary.io
* https://github.com/davidesantangelo/feedi
* https://simon04.dev.openstreetmap.org/whodidit/
  - generates RSS feeds to OSM edits in selected region
  - could either use this or write something similar
  - https://www.openstreetmap.org/history#map=11/43.4489/-80.5099
  - https://wiki.openstreetmap.org/wiki/Feeds#Editing_activity_in_the_map_data
* https://civichallto.ca/grit-toronto/, civic project usability testing
* https://github.com/dabreegster/abstreet
* https://data.cityofchicago.org/
* https://opengrid.io/
* http://plenar.io/
* https://github.com/aaronpk/XRay
* https://plantogether.city/
* https://github.com/UDST/urbanaccess

### Scraping information

* https://github.com/johnallen3d/schedule*scrape
* https://blog.scrapinghub.com/2016/08/25/how-to-crawl-the-web-politely-with-scrapy/
* [QuickCode](https://quickcode.io/), formerly [ScraperWiki](https://en.wikipedia.org/wiki/ScraperWiki)
* [Portia](https://scrapinghub.com/portia/) visual web scraper

### Similar projects
* [Ground](https://ground.news/)
* [Forekast](https://forekast.com)
* [World-o-meters](https://www.worldometers.info/)
* [Its On Village](https://www.itsonvillage.com)
* [Whitley Pump](http://whitleypump.uk/)
* [Outline](http://scripting.com/2019/09/15/153025.html) for a similar project by Dave Wiener
* [Street Art Toronto](https://streetart.to/#)
* [MapCarta](https://mapcarta.com/Kitchener)
* [Space Finder - Waterloo Region](https://waterlooregion.spacefinder.org/)
* [dataloo](https://github.com/jeamsden/dataloo), [cycle overview](https://dataloo.herokuapp.com/cycling_overview)
* [Represent Civic Information API](https://represent.opennorth.ca/demo/) find
    your political representatives my location or postal code
* displaying universty source dependencies
  * [Questionably Accurate Course Scheduler(QUACS)](https://quacs.org/#/)
    - scraps university course data
    - generates static data for run the scheduling webapp
  * [University Ciriculum Maps](https://github.com/Nassim-Saboundji/UniversityCurriculumMaps)


